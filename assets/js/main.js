function add()
{
    $('div[name="add_div"]').css('display', 'block');
}

function add_submit()
{
    $.ajax({
		async:true,
		type: "POST",
		url: SITE_URL+'/add_item',
		data:{
			spend_item: $('[name="spend_item"]').val(),
            money: $('[name="money"]').val(), 
            spend_date: $('[name="spend_date"]').val() 
		},
		dataType: "json",
		success: function(result)
		{
			if(result.CODE==100) {
                $('[name="tb_list"]').find('tbody').eq(0)
                .append('<tr>'+
                        '<td>'+$('[name="spend_item"]').val()+'</td>'+
                        '<td>$'+$('[name="money"]').val()+'</td>'+
                        '<td>'+$('[name="spend_date"]').val()+'</td>'+
                        '</tr>');
                $('div[name="add_div"]').find('input').val('');
                $('div[name="add_div"]').css('display', 'none');
			} else {
				console.log('failed');
			}
		}
	});
}