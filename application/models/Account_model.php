<?php

class Account_model extends CI_Model {

    public function __construct()
    {
        $this->load->database(); //load database library
    }

    public function spend_item_list()
    {
        $query = $this->db->query('SELECT * FROM tb_spend_item WHERE IS_DELETE = 0');
        return $query->result_array();
    }

    public function add_item($spend_item, $money, $spend_date)
    {
        $sql = 'INSERT INTO tb_spend_item (ITEM_NAME, MONEY, SPEND_DATE) VALUES (\''.$spend_item.'\', '.$money.', STR_TO_DATE(\''.$spend_date.'\', \'%Y-%m-%d\'))';
        $query = $this->db->query($sql);
        return $query;
    }
}

?>