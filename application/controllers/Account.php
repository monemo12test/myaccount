<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Account extends CI_Controller {

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see https://codeigniter.com/user_guide/general/urls.html
	 */
	public function index()
	{

	}

	public function spend_item_list()
	{
		$data = array();
		$this->load->model('Account_model'); // load model from application/models
		$spend_item_list = $this->Account_model->spend_item_list(); // call function spend_item_list() from Account_model
		$data['list'] = $spend_item_list; // put $spend_item_list into variable named 'list' in view
		$this->load->view('Account/index', $data); // send $data to view
	}

	public function add_item()
	{
		$this->load->model('Account_model'); // load model from application/models
		$result = $this->Account_model->add_item($this->input->post('spend_item'), $this->input->post('money'), $this->input->post('spend_date'));
		
		if($result == 1) {
			$code = 100;
			$msg = 'success';
		} else {
			$code = 101;
			$msg = 'failed';
		}

		$reseponse=array(
			"CODE" => $code,
			"ReturnString" => '',
			"MSG" => $msg
		);
		echo json_encode( $reseponse );
		exit;
	}
}
