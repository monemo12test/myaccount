<!DOCTYPE html>
<html lang="en">
    <!-- start Header  -->
    <head>
        <title></title>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <link href="<?php echo base_url('assets/css/bootstrap.min.css'); ?>" rel="stylesheet">
        <script>
        var SITE_URL = "<?php echo site_url(); ?>";
        var BASE_URL = "<?php echo base_url(); ?>";    
        </script>
        <script src="<?php echo base_url('assets/js/jquery-3.2.1.min.js'); ?>"></script>
        <script src="<?php echo base_url('assets/js/bootstrap.min.js'); ?>"></script>
        <script src="<?php echo base_url('assets/js/main.js'); ?>"></script>
    </head>
    <!-- Header end  -->
    <body>
        <h1>Hello Account</h1>
            <table style="width:100%" name="tb_list">
                <thead>
                    <td>項目</td>
                    <td>金額</td>
                    <td>日期</td>
                </thead>
                <tbody>
                    <?php foreach($list as $key => $item) : ?>
                    <tr>
                        <td><?php echo $item['ITEM_NAME'];?></td>
                        <td>$<?php echo $item['MONEY'];?></td>
                        <td><?php echo $item['SPEND_DATE'];?></td>
                    </tr>
                    <?php endforeach; ?>
                </tbody>    
            </table>
            <button onclick="add();">新增資料</button>
            <div style="display:none;" name="add_div">
                <input type="text" name="spend_item" placeholder="項目">
                <input type="number" name="money"  placeholder="金額">
                <input type="date" name="spend_date" placeholder="日期">
                <button onclick="add_submit();">送出</button>
            </div>
    </body>
</html>